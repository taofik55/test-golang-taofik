package repository

import (
	"fmt"
	"test-backend-taofik/driver"
	"test-backend-taofik/models"
	"golang.org/x/crypto/bcrypt"
)

// RESPONSE MODEL
type ResponseModel struct {
	Code    int    `json: "code" validate: "required"`
	Message string `json: "message" validate: "required"`
}

// CRUD

// READ ALL
func ReadAll(Limit int, Offset int) []models.UserModel {
	db, err := driver.ConnectDB()

	if err != nil {
		fmt.Println(err.Error())
		return nil
	}

	defer db.Close()

	var result []models.UserModel
	var page = (Offset - 1) * Limit

	items, err := db.Query("SELECT * FROM users LIMIT ? OFFSET ?", Limit, page)

	if err != nil {
		fmt.Println(err.Error())
		return nil
	}

	fmt.Printf("%T\n", items)

	for items.Next() {
		var each = models.UserModel{}
		var err = items.Scan(&each.Id, &each.Username, &each.Password, &each.Name)

		if err != nil {
			fmt.Println(err.Error())
			return nil
		}

		result = append(result, each)
	}

	if err = items.Err(); err != nil {
		fmt.Println(err.Error())
		return nil
	}

	return result
}

// READ BY ID
func ReadId(Id int) []models.UserModel {
	db, err := driver.ConnectDB()

	if err != nil {
		fmt.Println(err.Error())
		return nil
	}

	defer db.Close()

	var result []models.UserModel

	items, err := db.Query(`SELECT * FROM users WHERE id = ?`, Id)

	if err != nil {
		fmt.Println(err.Error())
		return nil
	}

	fmt.Printf("%T", items)

	for items.Next() {
		var each = models.UserModel{}
		var err = items.Scan(&each.Id, &each.Username, &each.Password, &each.Name)

		if err != nil {
			fmt.Println(err.Error())
			return nil
		}

		result = append(result, each)

		if err != nil {
			fmt.Println(err.Error())
			return nil
		}
	}
	return result
}

// CREATE
func Create(L *models.UserModel) *ResponseModel {
	Res := &ResponseModel{500, "Internal Server Error"}
	bytes, err := bcrypt.GenerateFromPassword([]byte(L.Password), 14)

	db, err := driver.ConnectDB()

	if err != nil {
		fmt.Println(err.Error())
		return Res
	}

	cryptedPass := string(bytes)

	defer db.Close()

	if (len(L.Username)) >= 3 {
		if (len(L.Password)) >= 7 {
			if (len(L.Name)) >= 3 {
				_, err = db.Exec(`INSERT INTO users (username, password, name) VALUES (?, ?, ?)`, L.Username, cryptedPass, L.Name)
			} else {
				Res = &ResponseModel{500, "Name must more than 3 chars"}
				return Res
			}
		} else {
			Res = &ResponseModel{500, "Password must more than 7 chars"}
			return Res
		}
	} else {
		Res = &ResponseModel{500, "Username must more than 3 chars"}
		return Res
	}

	if err != nil {
		fmt.Println(err.Error())
		Res = &ResponseModel{400, "Failed to Save Data, Username is exist!!!"}
		return Res
	}

	fmt.Println("Insert Success!")
	Res = &ResponseModel{200, "Success Save Data"}
	return Res
}

// DELETE
func Delete(id int) *ResponseModel {
	Res := &ResponseModel{500, "Internal Server Error"}

	db, err := driver.ConnectDB()

	if err != nil {
		fmt.Println(err.Error())
		return Res
	}

	defer db.Close()

	_, err = db.Exec(`DELETE FROM users WHERE id = ?`, id)

	if err != nil {
		fmt.Println(err.Error())
		Res = &ResponseModel{400, "Failed to Delete Data"}
		return Res
	}

	fmt.Println("Delete Success!")
	Res = &ResponseModel{200, "Success Delete Data"}
	return Res
}

// UPDATE
func Update(L *models.UserModel, id int) *ResponseModel {
	Res := &ResponseModel{500, "Internal Server Error"}
	bytes, err := bcrypt.GenerateFromPassword([]byte(L.Password), 14)

	db, err := driver.ConnectDB()

	if err != nil {
		fmt.Println(err.Error())
		return Res
	}

	defer db.Close()

	cryptedPass := string(bytes)

	if (len(L.Username)) >= 3 {
		if (len(L.Password)) >= 7 {
			if (len(L.Name)) >= 3 {
				_, err = db.Exec("UPDATE users SET username = ?, password = ?, name = ? WHERE id = ?", L.Username, cryptedPass, L.Name, id)
			} else {
				Res = &ResponseModel{500, "Name must more than 3 chars"}
				return Res
			}
		} else {
			Res = &ResponseModel{500, "Password must more than 7 chars"}
			return Res
		}
	} else {
		Res = &ResponseModel{500, "Username must more than 3 chars"}
		return Res
	}

	if err != nil {
		fmt.Println(err.Error())
		Res = &ResponseModel{400, "Failed to Update Data, Username is Exist"}
		return Res
	}

	fmt.Println("Update Success!")
	Res = &ResponseModel{200, "Success Update Data"}
	return Res
}
