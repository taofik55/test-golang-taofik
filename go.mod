module example.com/m

go 1.15

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible // indirect
	github.com/go-sql-driver/mysql v1.6.0
	github.com/labstack/echo v3.3.10+incompatible
	github.com/labstack/gommon v0.3.0 // indirect
	github.com/lib/pq v1.10.1
	github.com/prprprus/scheduler v0.5.0 // indirect
	golang.org/x/crypto v0.0.0-20210505212654-3497b51f5e64 // indirect
)
