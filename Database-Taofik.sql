/*
 Navicat Premium Data Transfer

 Source Server         : MySQLTaofik
 Source Server Type    : MySQL
 Source Server Version : 80024
 Source Host           : localhost:3306
 Source Schema         : test

 Target Server Type    : MySQL
 Target Server Version : 80024
 File Encoding         : 65001

 Date: 06/05/2021 15:23:39
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `username` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `password` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uc_username`(`username`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (1, 'aaaa', 'asd213', 'bfxcvxcggdfg');
INSERT INTO `users` VALUES (5, 'Aslina', '121233123', 'bfggdfg');
INSERT INTO `users` VALUES (8, 'data1', '121233123', 'bfggdfg');
INSERT INTO `users` VALUES (10, 'data2', '121233123', 'bfggdfg');
INSERT INTO `users` VALUES (12, 'data3', '121233123', 'bfggdfg');
INSERT INTO `users` VALUES (13, 'data4', '121233123', 'bfggdfg');
INSERT INTO `users` VALUES (14, 'data5', '$2a$14$M0cvmyD.bpDMYK/vh5LaiegZr3iBUhuSXdAcdcAsWrKpor9Q984rO', 'bfggdfg');
INSERT INTO `users` VALUES (15, 'data6', '$2a$14$FRPqrwJJxPpf9OByiHO/zuM4/qvGF05iD9HQ2kxTqk/Xs7k5ZTxwe', 'bfggdfg');
INSERT INTO `users` VALUES (16, 'dataCripted1', '$2a$14$usF5QE/yUjQq9pJIEGT6aeiok64HoekFxy9SrsNDhDBM0LDAz0Nqq', 'Cripted');
INSERT INTO `users` VALUES (17, 'dataCripted2Updated', '$2a$14$TbpPvaekIbixCrKdVPng/Og.2Y3Xc6ZFNxpylfHvMi6mhF12O8iMu', 'Cripted');

SET FOREIGN_KEY_CHECKS = 1;
