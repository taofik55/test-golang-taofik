package routes

import (
	"test-backend-taofik/service"

	"github.com/labstack/echo"
)

func EndPoint() {
	e := echo.New()

	// ROLES ENDPOINT
	e.GET("user/:limit/:offset", service.ReadAll)
	e.GET("user/:Id", service.ReadId)
	e.POST("user/", service.Create)
	e.PUT("user/:Id", service.Update)
	e.DELETE("user/:Id", service.Delete)

	e.Logger.Fatal(e.Start(":1323"))
}
