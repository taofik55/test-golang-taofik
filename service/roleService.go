package service

import (
	"net/http"
	"strconv"
	"test-backend-taofik/models"
	"test-backend-taofik/repository"

	"github.com/labstack/echo"
)

type ResponseModel struct {
	Code    int    `json:"code" validate:"required"`
	Message string `json:"message" validate:"required"`
}

//ReadAllRole function
func ReadAll(c echo.Context) error {
	id1 := c.Param("limit")
	id2 := c.Param("offset") 
	data1, _ := strconv.Atoi(id1)
	data2, _ := strconv.Atoi(id2)
	result := repository.ReadAll(data1, data2)
	return c.JSON(http.StatusOK, result)
}

func Create(c echo.Context) error {
	Res := &ResponseModel{400, "Bad Request"}
	U := new(models.UserModel)
	if err := c.Bind(U); err != nil {
		return nil
	}
	Res = (*ResponseModel)(repository.Create(U))
	return c.JSON(http.StatusOK, Res)
}

func ReadId(c echo.Context) error {
	id := c.Param("Id")
	data, _ := strconv.Atoi(id)
	result := repository.ReadId(data)
	return c.JSON(http.StatusOK, result)
}

func Delete(c echo.Context) error {
	Res := &ResponseModel{400, "Bad Request"}
	id := c.Param("Id")
	data, _ := strconv.Atoi(id)
	Res = (*ResponseModel)(repository.Delete(data))
	return c.JSON(http.StatusOK, Res)
}

func Update(c echo.Context) error {
	Res := &ResponseModel{400, "Bad Request"}
	id := c.Param("Id")
	data, _ := strconv.Atoi(id)
	U := new(models.UserModel)
	if err := c.Bind(U); err != nil {
		return nil
	}
	Res = (*ResponseModel)(repository.Update(U, data))
	return c.JSON(http.StatusOK, Res)
}
